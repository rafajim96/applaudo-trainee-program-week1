# Setup #

First you have to clone the project, all the .rb files for this project are on the develop branch, the file with all the methods is on lib/week1.rb


### Problem 1 ###
There are two variants for this problem:



* ### alpha_series ###

Receives: Does not receives a parameter.

What it does: Prints an alphabetical series of 2 chars.

Returns: Nothing.

How to run: Call the method. Example: 'alpha_series()'


* ### alpha_series(integer) ###

Receives: An Integer N that represents the maximum quantity of characters that the series will have

What it does: Prints an alphabetical series of N chars.

Returns: Nothing.

How to run: Call the method and provide an Integer. Example: 'alpha_series(5)'


### Problem 2 ###


* ### to_histogram(array) ###

Receives: An array of numeric values.

What it does: It creates and prints a histogram based on the input array.

Returns: A Hash that represents the histogram or an empty array if the input array is empty.

How to run: Call the method and provide an array of numeric values. Example: 'to_histogram([1,1,2,2,3,3,3,4,5,6,6])'


### Problem 3 ###


* ### mean(array) ###

Receives: An array of numeric values.

What it does: Calculates and prints the mean of the input array.

Returns: A float value that represents the mean of the array or an empty array if the input array is empty.

How to run: Call the method and provide an array of numeric values. Example: 'mean([3,5,8,6,4,2])'


* ### median(array) ###

Receives: An array of numeric values.

What it does: Calculates and prints the median of any array. (even or odd)

Returns: A Float that represents the median in case of an even array or an Integer that represents the median in case of an odd array or an empty array if the input array is empty.

How to run: Call the method and provide an array of numeric values. Example: 'median([1,1,2,2,3,3,3,4,5,6,6])'


* ### mode(array) ###

Receives: An array of numeric values.

What it does: Calculates and prints the mode of the input array.

Returns: An array of numeric values that contains the mode or mode's in case that there is more than one or an empty array if the input array is empty.

How to run: Call the method and provide an array of numeric values. Example: 'mode([1,1,2,2,3,3,3,4,5,6,6])'


### Miscellaneous ###

I created a function to validate if an array contains only numeric values, which is used on the Problems #2 and #3 functions, with the purpouse of avoiding repeated code.


* ### check_numeric_array(array) ###

Receives: An array of numeric values.

What it does: Validates if an array contains only numeric values.

Returns: True if all the values on the array are numeric or False if there is at least one non-numeric value.


